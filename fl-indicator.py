#!/usr/bin/env python
# -*- coding: utf-8 -*-

#  		@version: 0.1.5.10
#  		@author : Beliakov Vladislav [kolerii]
import sys
import gtk
import appindicator
from bs4 import BeautifulSoup
import urllib
import re
import subprocess
import string

PING_FREQUENCY = 200

def strip_tags(html, exceptions=None):
	def repl(match):
		matched = match.group(0)
		tag = matched.replace('/', '')
		if exceptions and tag in exceptions:
			return matched
	return re.sub("<[^>]+>", repl, html).strip()

class FLChecker:

	def __init__(self):
		self.ind = appindicator.Indicator("fl-indicator", "indicator-messages", appindicator.CATEGORY_APPLICATION_STATUS)
		self.ind.set_status(appindicator.STATUS_ACTIVE)
		self.ind.set_attention_icon("new-messages-red")

		self.last_url = False
		self.data = {}
		
		#self.keywords = ['парсер','скрипт','php','1C','wordpress','dle','парсинг','js','jquery','python','пхп','питон']
		self.keywords = ['парсер']

		for x in self.keywords:
			self.data[x] = {'last_url': False}

		self.menu_setup()
		self.setup_window_projects()

		self.ind.set_menu(self.menu)

	def get_page( self, url ):
		try:
			return ''.join(urllib.urlopen( url ))
		except:
			return False

	def menu_setup(self):
		self.menu = gtk.Menu()

		self.show_window_projects_item = gtk.MenuItem('Все проекты')
		self.show_window_projects_item.connect('activate', self.show_window_projects)
		self.show_window_projects_item.show()
		self.menu.append(self.show_window_projects_item)

		self.check_item = gtk.MenuItem('Снять выделение')
		self.check_item.connect('activate', self.check_new)
		self.check_item.show()
		self.menu.append(self.check_item)

		self.quit_item = gtk.MenuItem('Закрыть')
		self.quit_item.connect("activate", self.quit)
		self.quit_item.show()
		self.menu.append(self.quit_item)

	def send_notify(self, title, message):
		subprocess.Popen(['notify-send', title, message])
		return

	def check_new( self , w ):
		self.ind.set_status(appindicator.STATUS_ACTIVE)
		return

	def main(self):
		self.get_last_project_parsers()
		gtk.timeout_add(PING_FREQUENCY * 1000, self.get_last_project_parsers)
		gtk.main()

	def quit(self, widget):
		sys.exit(0)

	def setup_window_projects( self ):
		#Window "Все проекты"
		self.window_projects = gtk.Window()
		self.window_projects.connect('delete-event', self.clear_window_projects)
		self.window_projects.set_title('Все проекты')
		self.window_projects.set_icon_from_file('FL_logo.png')
		self.window_projects.set_size_request(1000,450)

		table = gtk.Table(10, 6, True)
		self.window_projects.add(table)

		self.window_projects.notebook = gtk.Notebook()
		self.window_projects.notebook.set_tab_pos(gtk.POS_LEFT)
		self.window_projects.notebook.set_scrollable(True)
		table.attach(self.window_projects.notebook, 0, 3, 0, 10)

		self.window_projects.display_desc_view = gtk.TextView()
		scrolled_window = gtk.ScrolledWindow()
		scrolled_window.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.window_projects.display_desc = gtk.TextBuffer()
		self.window_projects.display_desc_view.set_editable(False)
		self.window_projects.display_desc_view.set_cursor_visible(False)
		self.window_projects.display_desc_view.set_wrap_mode(gtk.WRAP_WORD)
		self.window_projects.display_desc_view.set_buffer(self.window_projects.display_desc)
		table.attach(scrolled_window, 3, 6, 0, 9)
		scrolled_window.add_with_viewport(self.window_projects.display_desc_view)
		scrolled_window.show()

		self.window_projects.label_url = gtk.Label()
		self.window_projects.label_price = gtk.Label()
		table.attach(self.window_projects.label_url, 5, 6, 9, 10)
		table.attach(self.window_projects.label_price, 3, 5, 9, 10)

	def print_desc_project( self , w ):
		(model, pathlist) = w.get_selection().get_selected_rows()
		if pathlist:
			url = model.get_value(model.get_iter(pathlist[0]),1)
			(desc,cost) = self.get_desc_project(url)
			self.window_projects.display_desc.set_text(strip_tags( desc , ['br','a'] ))
			self.window_projects.label_url.set_markup('<a href="'+url+'" title="Перейти на страницу FL.ru">Смотреть на FL.ru</a>')
			self.window_projects.label_price.set_label('Стоимость: ' + strip_tags(cost))

	def clear_window_projects( self , w, i ):
		while self.window_projects.notebook.get_current_page() > -1:
			self.window_projects.notebook.remove_page(-1)

		self.window_projects.label_price.set_label('')
		self.window_projects.label_url.set_label('')
		self.window_projects.display_desc.set_text('')

		self.setup_window_projects()

	def show_window_projects(self, widget):
		while self.window_projects.notebook.get_current_page() > -1:
			self.window_projects.notebook.remove_page(-1)

		self.ind.set_status(appindicator.STATUS_ACTIVE)
		for keyword in self.data:
			label = gtk.Label()
			label.set_label(keyword)
			listprojects = gtk.ListStore(str, str)
			for link in self.data[keyword]['links']:
				listprojects.append([link['title'],link['url']])
			listprojects_view = gtk.TreeView(model=listprojects)
			listprojects_view.connect('cursor-changed',self.print_desc_project)
			renderer_text = gtk.CellRendererText()
			renderer_text.set_property('size-points',8)
			column_text = gtk.TreeViewColumn("Заголовок", renderer_text, text=0)
			listprojects_view.append_column(column_text)

			self.window_projects.notebook.append_page(listprojects_view,label)
			self.window_projects.show_all()

	def get_last_project_parsers(self):
		for keyword in self.data.keys():
			data = self.get_list_project(keyword)
			if data:
				if self.data[keyword]['last_url'] is not False and self.data[keyword]['last_url'] != data[0]['url']:
					self.send_notify('Новый проект по запросу "'+keyword+'"',data[0]['title'])
					self.ind.set_status(appindicator.STATUS_ATTENTION)
					self.data[keyword]['last_url'] = data[0]['url']
				elif self.data[keyword]['last_url'] is False:
					self.send_notify('Последнее задание по запросу "'+keyword+'"',data[0]['title'])
					self.data[keyword]['last_url'] = data[0]['url']
				self.data[keyword]['links'] = data
		if self.window_projects.is_active():
			self.show_window_projects('')
		return True

	def get_list_project(self, keyword):
		url = 'https://www.fl.ru/search/?action=search&type=projects&search_elms%5Bprojects%5D=0&search_string='+urllib.quote_plus(keyword.encode('cp1251'))+'&search_hint=%D1%86%D0%B8%D1%84%D1%80%D0%BE%D0%B2%D0%B0%D1%8F+%D0%B6%D0%B8%D0%B2%D0%BE%D0%BF%D0%B8%D1%81%D1%8C&limit_page=0&pf_cost_from=&pf_cost_to=&pf_currency=2&pf_category=0&pf_subcategory=0&pf_country=0&pf_city=0'
		projects = []
		page = self.get_page( url )
		if page is not False:
			soup = BeautifulSoup(page)
			ret = soup.findAll('div', attrs={'class':'search-lenta-item'})
			for x in ret:
				a = x.find('a')
				title = ''.join([e for e in a.recursiveChildGenerator() if isinstance(e,unicode)]).replace('','-')
				url   = 'https://www.fl.ru' + a['href']
				projects.append({'title':title,'url':url})
		return projects

	def get_desc_project( self, url ):
		page = self.get_page( url )
		project_id = string.split(url,'/')[4]
		print project_id
		if page is not False:
			replace_tags = {'<br/>':'\n','<br/><br/>':'\n','  ':' ','&gt;':'>','&lt;':'<','&amp;':'&','–':'-','':'-'}
			soup = BeautifulSoup(page)
			desc = re.sub("|".join(map(re.escape, replace_tags.keys())), lambda m: replace_tags[m.group()], str(soup.find('div', attrs={'id':'projectp'+project_id}))).strip()
			cost = soup.find('span', attrs={'class':'b-layout__bold'})
			if cost is None:
				cost = 'По договоренности'
			else:
				cost = str(cost.string).strip()
			return (desc,cost)
		else:
			return False

if __name__ == "__main__":
	indicator = FLChecker()
	indicator.main()